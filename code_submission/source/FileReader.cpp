#include <iostream>
#include <fstream>
#include <string>

#define DEBUG 1

int main(int argc, char **argv)
{
   // Validate number of command line arguments
   //    If failure, inform user and provide usage information
   //    If successful, check if file exists
   //       If failure, inform user that file could not be found in current dir
   //       If successful, read contents of the file
   //          Check for correct formatting
   //             If successful, serialise and prepare for computation
   //             If unsuccesful, inform user and offer to generate a README

   #if DEBUG
      std::cout<<"DB: Debug mode is on; to disable it access "
      "'source/FileReader.cpp' and edit the 'DEBUG'\n"
      "macro variable\n"
      "===================================================="
      <<std::endl;
   #endif

   // Validate number of command line arguments
   if(argc==2)
   // If successful, check if file exists
   {
      #if DEBUG
         std::cout<<"DB: Correct number of command-line "
         "arguments"<<std::endl;
      #endif
      
      std::ifstream input_file(argv[1]);
      if(input_file.good())
      // If successful, read contents of the file
      {
         #if DEBUG
            std::cout<<"DB: File found"<<std::endl;
         #endif

         std::string line;
         // TEMP: Read text file contents
         while(std::getline(input_file, line))
         {
            std::cout<<line<<std::endl;
         }

         // Check for correct formatting
         std::getline(input_file, line);
         

         input_file.close();
      }
      else
       // If failure, inform user that file could not be found in current dir
      {
         std::cout<<"The specified file could not be found. Please ensure that the path\n"
         "provided is relative to the current location of this executable and try again"
         <<std::endl;
         return 0;
      }
      

      return 1;
   }
   else
   // If failure, inform user and provide useage information
   {
      #if DEBUG
         std::cout<<"DB: Incorrect number of command-line "
         "arguments"<<std::endl;
      #endif
      std::cout<<"Usage: ./FileReader <file_name>"<<std::endl;
      return 0;
   }
}