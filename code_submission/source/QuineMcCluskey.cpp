#include <iostream>
#include <vector>
#include <algorithm>

#define DEBUG 1

#if 1
std::vector<std::vector<int>> example_input = {
    {4, 1},
    {0, 0, 0, 0, 0},
    {0, 0, 0, 1, 0},
    {0, 0, 1, 0, 0},
    {0, 0, 1, 1, 0},
    {0, 1, 0, 0, 1},
    {0, 1, 0, 1, 0},
    {0, 1, 1, 0, 0},
    {0, 1, 1, 1, 0},
    {1, 0, 0, 0, 1},
    {1, 0, 0, 1, -1},
    {1, 0, 1, 0, 1},
    {1, 0, 1, 1, 1},
    {1, 1, 0, 0, 1},
    {1, 1, 0, 1, 0},
    {1, 1, 1, 0, -1},
    {1, 1, 1, 1, 1}};
#endif

// Method to count number of 1s in a vector; used later to clasify inputs
int number_of_1s(std::vector<int> vector_in)
{
   int no = 0;
   for (std::vector<int>::iterator it = vector_in.begin(); it != vector_in.end(); ++it)
   {
      if (*it == 1)
      {
         ++no;
      }
   }
   return no;
}

// Method to print a vector (used mostly for debugging)
void print_vector(std::vector<int> vector_in)
{
   std::vector<int>::iterator end = vector_in.end();
   for (std::vector<int>::iterator it = vector_in.begin(); it != end; ++it)
   {
      if (*it == -1)
      {
         std::cout << " _";
      }
      else
      {
         std::cout << " " << *it;
      }
   }
}

// Tests two vector to see if they are compatible and can be combined; returns True if they can, False otherwise
bool compatible_vectors(std::vector<int> v1, std::vector<int> v2, int diff_allowed)
{
   int v_length = v1.size();
   if (v2.size() != v_length)
   {
      return false;
   }
   int diff = 0;
   for (int i = 0; i < v_length; ++i)
   {
      if (v1[i] != v2[i])
      {
         if (v1[i] == -1 || v2[i] == -1)
         {
            return false;
         }
         ++diff;
      }
   }
   if (diff > diff_allowed)
   {
      return false;
   }
   else
   {
      return true;
   }
}

// Attempts to combine two vectors according to the QM algorithm; returns the combined vector
std::vector<int> combine_vectors(std::vector<int> v1, std::vector<int> v2)
{
   std::vector<int> v_out = v1;
   for (int i = 0; i < v1.size(); ++i)
   {
      if (v1[i] != v2[i])
      {
         v_out[i] = -1;
      }
   }
   return v_out;
}

int main()
{
   std::vector<std::vector<int>> file_input = example_input;
   int input_size = file_input.size() - 1;
   // std::cout << "Size: " << input_size << std::endl;

#if DEBUG
   std::cout << "DB: Debug mode is on; to disable it access"
                "'source/FileReader.cpp' and edit the 'DEBUG'\n"
                "macro variable\n"
                "===================================================="
             << std::endl;
#endif

   std::vector<int> input_meta = file_input[0];
   int file_in_len = file_input.size() - 1;

#if DEBUG
   std::cout << "DB: Input meta-data:\nNo. of input variables: "
             << input_meta[0] << "\nNo. of output variables: "
             << input_meta[1] << "\n===================================================="
             << std::endl;
#endif

   // Separate input file's actual inputs by 'slicing' the input file
   std::vector<std::vector<int>> inputs;
   std::vector<std::vector<int>> outputs;
   for (int i = 1; i <= input_size; ++i)
   {
      // 'Slice' occurs here
      inputs.push_back(std::vector<int>(file_input[i].begin(), file_input[i].begin() + input_meta[0]));
      outputs.push_back(std::vector<int>(file_input[i].begin() + input_meta[0], file_input[i].end()));
   }

   // Steps to generate minterms for the first iteration of the QM algorithm;
   int current_iteration = 0;
   std::vector<std::vector<int>> current_iteration_inputs = inputs;
   std::vector<int> current_iteration_outputs;
   // Take only ith set of outputs
   for (std::vector<std::vector<int>>::iterator it = outputs.begin(); it != outputs.end(); ++it)
   {
      current_iteration_outputs.push_back((*it)[current_iteration]);
   }

   // DEBUG: Print the current output vector
#if DEBUG
   for (std::vector<int>::iterator it = current_iteration_outputs.begin(); it != current_iteration_outputs.end(); ++it)
   {
      std::cout << " " << *it;
   }
   std::cout << std::endl;
#endif

   // Delete rows of the inputs that have 0s in the Outputs column
   for (int i = input_size; i >= 0; --i)
   {
      if (current_iteration_outputs[i] == 0)
      {
         current_iteration_inputs.erase(current_iteration_inputs.begin() + i);
      }
   }

// DEBUG: Print the current input vectors
#if DEBUG
   for (std::vector<std::vector<int>>::iterator it = current_iteration_inputs.begin(); it != current_iteration_inputs.end(); ++it)
   {
      print_vector(*it);
      std::cout << std::endl;
   }
#endif

   // Iterate over the input groups and test + combine items from neighboring groups
   std::vector<std::vector<int>> next_iteration_minterms;
   std::vector<std::vector<int>> minterm_checks;
   std::vector<std::vector<int>> unchecked_minterms;
   std::vector<std::vector<std::vector<int>>> grouped_minterms;

   bool continue_iterating = true;
   //while (continue_iterating) REDUNDANT
   for (int count = 0; /*count < 3 REDUNDANT*/ continue_iterating == true; ++count)
   {
#if DEBUG
      std::cout << "=================================================="
                << "Iteration " << count << " begin" << std::endl
                << std::endl;
#endif
      // Iteration Begin
      // Inputs are compared, combined, and passed to outputs
      // Group the newly pruned inputs and create the iteration's 'check' and 'prime implicant' vectors

      for (int i = 0; i <= input_meta[0]; ++i)
      {
         grouped_minterms.push_back(std::vector<std::vector<int>>());
         minterm_checks.push_back(std::vector<int>());
      }

      for (std::vector<std::vector<int>>::iterator it = current_iteration_inputs.begin(); it < current_iteration_inputs.end(); ++it)
      {
         int temp_1s_group = number_of_1s(*it);
         grouped_minterms[temp_1s_group].push_back(*it);
         minterm_checks[temp_1s_group].push_back(0);
      }

// DEBUG: Check all inputs are correctly grouped
// NEW: Also check that checklist is correctly initialised
#if DEBUG
      std::cout << "Input groups:" << std::endl;
      int i = 0;
      for (std::vector<std::vector<std::vector<int>>>::iterator it = grouped_minterms.begin(); it != grouped_minterms.end(); ++it)
      {
         std::cout << "Group " << i << std::endl;
         if ((*it).size() == 0)
         {
            std::cout << "Empty group" << std::endl;
         }
         else
         {
            for (std::vector<std::vector<int>>::iterator itt = (*it).begin(); itt != (*it).end(); ++itt)
            {
               print_vector(*itt);
               std::cout << std::endl;
            }
         }
         ++i;
      }

      std::cout << "Checklist:" << std::endl;
      std::cout << "Length = " << minterm_checks.size() << std::endl;
      for (std::vector<std::vector<int>>::iterator it = minterm_checks.begin(); it < minterm_checks.end(); ++it)
      {
         print_vector(*it);
         std::cout << std::endl;
      }
#endif

      // Iterate over the Groups:
      for (int i = 0; i < grouped_minterms.size() - 1; ++i)
      {
         if (grouped_minterms[i].size() != 0 && grouped_minterms[i + 1].size() != 0)
         {
            // Item in first group:
            for (int j = 0; j < grouped_minterms[i].size(); ++j)
            {
               // Item in second group:
               for (int k = 0; k < grouped_minterms[i + 1].size(); ++k)
               {
                  // Compare + combine if good
                  if (compatible_vectors(grouped_minterms[i][j], grouped_minterms[i + 1][k], count + 1))
                  {
                     next_iteration_minterms.push_back(combine_vectors(grouped_minterms[i][j], grouped_minterms[i + 1][k]));
                     minterm_checks[i][j] = 1;
                     minterm_checks[i + 1][k] = 1;
                  }
               }
            }
         }
      }

// DEBUG: Check output + checked-off inputs
#if DEBUG
      std::cout << "Checklist:" << std::endl;
      std::cout << "Length = " << minterm_checks.size() << std::endl;
      for (std::vector<std::vector<int>>::iterator it = minterm_checks.begin(); it < minterm_checks.end(); ++it)
      {
         print_vector(*it);
         std::cout << std::endl;
      }
#endif

      // Append any inputs that have not been checked off to the unchecked_minterms vector, move the next_iteration_minterms to the current_iteration_inputs, and increment the current iteration
      continue_iterating = false;
      for (int i = 0; i < minterm_checks.size(); ++i)
      {
         for (int j = 0; j < minterm_checks[i].size(); ++j)
         {
            if (minterm_checks[i][j] == 0)
            {
               unchecked_minterms.push_back(grouped_minterms[i][j]);
            }
            else
            {
               continue_iterating = true;
            }
         }
      }

      // Reset everything for next iteration
      current_iteration_inputs.clear();
      grouped_minterms.clear();
      minterm_checks.clear();
      current_iteration_inputs = next_iteration_minterms;
      next_iteration_minterms.clear();

      // Remove duplicate minterms from next iteration
      std::sort(current_iteration_inputs.begin(), current_iteration_inputs.end());
      current_iteration_inputs.erase(std::unique(current_iteration_inputs.begin(), current_iteration_inputs.end()), current_iteration_inputs.end());

#if DEBUG
      std::cout << "Next iteration minterms:" << std::endl;
      for (std::vector<std::vector<int>>::iterator it = current_iteration_inputs.begin(); it < current_iteration_inputs.end(); ++it)
      {
         print_vector(*it);
         std::cout << std::endl;
      }
#endif

      // Iteration End
      // If there are no checked minterms in the next_iteration_minterms, this process ends
      //
   }

#if DEBUG
   // Final prime implicants
   std::cout << "========================================"
             << std::endl
             << "Prime implicants: " << std::endl;
   for (std::vector<std::vector<int>>::iterator it = unchecked_minterms.begin(); it != unchecked_minterms.end(); ++it)
   {
      print_vector(*it);
      std::cout << std::endl;
   }
#endif

   // TODO: Identify essential prime implicants
   //    Take each 1 from the original inputs and see which prime implicants "fit" (1s and 0s match)

   // TODO: Combine essential prime implicants with non-essential prime implicants using Petrick's method to create the final output
   // TODO: Output to a file
   // TODO: Input formatting and input link to this algorithm

   return 0;
}